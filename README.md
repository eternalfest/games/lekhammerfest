# Lekhammerfest

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/lekhammerfest.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/lekhammerfest.git
```
