package lekhammerfest;

import etwin.Obfu;
import merlin.IAction;

@:build(patchman.Build.di())
class Lekhammerfest {

    @:diExport
    public var setFlag(default, null): IAction;


    public function new() {
        setFlag = new SetFlag();
    }
}
