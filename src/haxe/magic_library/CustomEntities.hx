package magic_library;

import hf.Hf;
import merlin.IAction;
import etwin.ds.FrozenArray;

import magic_library.entities.MovingPlatform;


@:build(patchman.Build.di())
class CustomEntities {
    @:diExport
    public var actions(default, null): FrozenArray<IAction>;

    public function new(hf: Hf, physicsOverrider: PlatformPhysicsOverride): Void {
        actions = FrozenArray.of(
                                 cast new MovingPlatformSpawn(hf, physicsOverrider),
                                 cast new MovingPlatformMover()
);
    }
}
