package magic_library.entities;

import etwin.flash.MovieClip;
import hf.entity.Physics;
import hf.entity.Trigger;
import hf.Hf;
import hf.levels.View;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import patchman.IPatch;
import etwin.Obfu;
import patchman.Ref;

class MovingPlatform extends MovieClip {
    private var maskTile: MovieClip;
    private var endTile: MovieClip;
    private var skin: MovieClip;

    private var game: GameMode;
    private var length: Float;

    public var dx: Float = 0;
    public var dy: Float = 0;
    public var minX: Float = 0;
    public var maxX: Float = 400;
    public var minY: Float = 0;
    public var maxY: Float = 500;

    private var platformPhysicsOverride: PlatformPhysicsOverride;

    @:keep
    public function new(): Void {
    }

    public function init(hf: Hf, g: GameMode, x: Float, y: Float, length: Int, platformPhysicsOverride: PlatformPhysicsOverride): Void {
        _x = x;
        _y = y;
        game = g;
        this.length = length;
        this.platformPhysicsOverride = platformPhysicsOverride;

        var skinId: Int = hf.levels.View.getTileSkinId(g.world.current.__dollar__skinTiles);
        maskTile._width = length * hf.Data.CASE_WIDTH;
        endTile._x = length * hf.Data.CASE_WIDTH;
        this.skin.gotoAndStop(Std.string(skinId));
        endTile.gotoAndStop(Std.string(skinId));

        /* For automatic call to update. */
        g.addToList(hf.Data.ENTITY, cast this);
    }

    public static function attach(hf: Hf, game: GameMode, x: Float, y: Float, length: Int, platformPhysicsOverride: PlatformPhysicsOverride): MovingPlatform {
        var platform: MovingPlatform = cast game.depthMan.attach(Obfu.raw("moving_platform"), hf.Data.DP_SPRITE_BACK_LAYER);
        platform.init(hf, game, x, y, length, platformPhysicsOverride);
        return platform;
    }

    private inline function getBeginX(): Float {
        return _x;
    }

    private inline function getEndX(): Float {
        return getBeginX() + 20 * length;
    }

    private inline function getBeginY(): Float {
        return _y;
    }

    private inline function getEndY(): Float {
        return _y + 20;
    }

    public inline function collidesWith(object: {x: Float, y: Float}): Bool {
        return object.x >= getBeginX() && object.x <= getEndX()
            && object.y >= getBeginY() && object.y <= getEndY();
    }

    @:keep
    public function update(): Void {
        var hf: Hf = game.root;
        _x += dx * hf.Timer.tmod;
        _y += dy * hf.Timer.tmod;

        for (player in game.getPlayerList()) {
            if (player.fl_kill || (cast player).linkedMovingPlatform != this) // TODO: Not in player ?
                continue;
            player.x += dx * hf.Timer.tmod;
            player.y = getBeginY();
            player.updateCoords();
            player.infix();
        }

        if (getBeginX() <= minX - 7 && dx < 0 || getEndX() >= maxX && dx > 0) {
            dx *= -1;
            // HACK To prevent vibrating in last level of Lekhammerfest
            if (minX == maxX)
                dx = 0;
        }
        if (getBeginY() <= minY - 20 && dy < 0 || getEndY() >= maxY && dy > 0) {
            dy *= -1;
            // HACK To prevent vibrating in last level of Lekhammerfest
            if (minY == maxY)
                dy = 0;
        }

    }

    @:keep
    public function destroy(): Void {
        platformPhysicsOverride.getMovingPlatforms().remove(this);
        removeMovieClip();
    }

    @:keep
    override public function onUnload() {
        game.removeFromList(game.root.Data.ENTITY, cast this);
    }
}

class MovingPlatformSpawn implements IAction {
    public var name(default, null): String = Obfu.raw("movingPF");
    public var isVerbose(default, null): Bool = false;

    private var platformPhysicsOverride: PlatformPhysicsOverride;

    public function run(ctx: IActionContext): Bool {
        var hf: Hf = ctx.getHf();
        var game: GameMode = ctx.getGame();

        var x: Float = game.flipCoordReal(hf.Entity.x_ctr(ctx.getFloat(Obfu.raw("x"))));
        x -= 10; // This is different from the AS mod, and I have no fucking clue why, I checked everything I oculd think of...
        var y: Float = hf.Entity.y_ctr(ctx.getFloat(Obfu.raw("y")));
        y -= 20;
        var length: Int = ctx.getInt(Obfu.raw("l"));
        var sid: Null<Int> = ctx.getOptInt(Obfu.raw("sid")).toNullable();

        if (game.fl_mirror)
            x -= (length - 1) * 20;

        var platform: MovingPlatform = MovingPlatform.attach(hf, game, x, y, length, platformPhysicsOverride);
        platformPhysicsOverride.addMovingPlatform(platform);
        ctx.killById(sid);
        (cast platform).scriptId = sid;
        ctx.registerMc(sid, platform);
        return false;
    }

    public function new(hf: Hf, platformPhysicsOverride: PlatformPhysicsOverride) {
        this.platformPhysicsOverride = platformPhysicsOverride;
        hf.Std.registerClass(Obfu.raw("moving_platform"), MovingPlatform);
    }
}

class MovingPlatformMover implements IAction {
    public var name(default, null): String = Obfu.raw("setMovingPF");
    public var isVerbose(default, null): Bool = false;

    // TODO: Change the way it's done. It's horrible to do properly and synchronize (cf. 10 of ML).
    public function run(ctx: IActionContext): Bool {
        var hf: Hf = ctx.getHf();
        var game: GameMode = ctx.getGame();

        var minX: Null<Float> = null;
        var minCellX: Null<Float> = ctx.getOptFloat(Obfu.raw("xmin")).toNullable();
        if (minCellX != null)
            minX = game.flipCoordReal(hf.Entity.x_ctr(minCellX));
        var maxX: Null<Float> = null;
        var maxCellX: Null<Float> = ctx.getOptFloat(Obfu.raw("xmax")).toNullable();
        if (maxCellX != null)
            maxX = game.flipCoordReal(hf.Entity.x_ctr(maxCellX));

        if (game.fl_mirror) {
            var tmp: Null<Float> = minX;
            minX = maxX;
            maxX = tmp;
        }

        var minY: Null<Float> = null;
        var minCellY: Null<Float> = ctx.getOptFloat(Obfu.raw("ymin")).toNullable();
        if (minCellY != null)
            minY = hf.Entity.y_ctr(minCellY);
        var maxY: Null<Float> = null;
        var maxCellY: Null<Float> = ctx.getOptFloat(Obfu.raw("ymax")).toNullable();
        if (maxCellY != null)
            maxY = hf.Entity.y_ctr(maxCellY);

        var speedX: Null<Float> = ctx.getOptFloat(Obfu.raw("sx")).toNullable();
        var speedY: Null<Float> = ctx.getOptFloat(Obfu.raw("sy")).toNullable();
        var sid: Int = ctx.getOptInt(Obfu.raw("sid")).toNullable();
        var platform: MovingPlatform = cast ctx.getMc(sid);
        if (speedX != null)
            platform.dx = speedX * (game.fl_mirror ? -1 : 1);
        if (speedY != null)
            platform.dy = speedY;
        if (minX != null)
            platform.minX = minX;
        if (maxX != null)
            platform.maxX = maxX;
        if (minY != null)
            platform.minY = minY;
        if (maxY != null)
            platform.maxY = maxY;
        return false;
    }

    public function new() {
    }
}

@:build(patchman.Build.di())
class PlatformPhysicsOverride {
    @:diExport
    public var rewritePhysics(default, null): IPatch;
    @:diExport
    public var unlinkPlatformFromPlayerOnMove(default, null): IPatch;
    @:diExport
    public var removePlatformsOnLevelEnd(default, null): IPatch;

    private var movingPlatforms: Array<MovingPlatform>;

    public function addMovingPlatform(platform: MovingPlatform): Void {
        movingPlatforms.push(platform);
    }

    // TODO: Probably not the best way...
    public function getMovingPlatforms(): Array<MovingPlatform> {
        return movingPlatforms;
    }

    private function entityCollidesWithAMovingPlatform(entity: Physics): Null<MovingPlatform> {
        for (platform in movingPlatforms) {
            if (platform.collidesWith(entity))
                return platform;
        }
        return null;
    }

    public function new(): Void {
        movingPlatforms = [];

        rewritePhysics = Ref.auto(Physics.update).replace(function(hf: Hf, self: Physics): Void {
            /* Copy paste of the original function with changes in the middle... I left a blank line around modified blocks. */

            Reflect.callMethod(self, untyped hf.entity.Animator.prototype.update, []); /* super.update(); */

            if (!self.fl_physics) {
                return;
            }
            self.updateCoords();
            if (self.fl_wind) {
                if (self.fl_stable && self.game.fl_wind) {
                    self.dx += self.game.windSpeed * hf.Timer.tmod;
                }
            }
            if (self.fl_stable) {

                var isStillOnMovingPlatform: Bool = false;
                var linkedMovingPlatform: Null<MovingPlatform> = (cast self).linkedMovingPlatform;
                if (linkedMovingPlatform != null)
                    isStillOnMovingPlatform = linkedMovingPlatform.collidesWith(self);
                if (self.dy != 0 || self.world.getCase({x: self.fcx, y: self.fcy}) != hf.Data.GROUND && !isStillOnMovingPlatform) {

                    self.fl_stable = false;
                }
            }
            if (self.dx != 0 || self.dy != 0 || !self.fl_stable) {
                if (!self.fl_skipNextGravity && !self.fl_stable && self.fl_gravity) {
                    var v4 = 1.0;
                    if (hf.Timer.tmod >= 2) {
                        v4 = 1.1;
                    }
                    if (self.dy < 0) {
                        self.dy += self.gravityFactor * hf.Data.GRAVITY * hf.Timer.tmod * v4;
                    } else {
                        if (self.fallStart == null) {
                            self.fallStart = self.y;
                        }
                        if (self.game.fl_aqua && !self.fl_strictGravity) {
                            self.dy += 0.3 * self.fallFactor * hf.Data.FALL_SPEED * hf.Timer.tmod * v4;
                        } else {
                            self.dy += self.fallFactor * hf.Data.FALL_SPEED * hf.Timer.tmod * v4;
                        }
                    }
                }
                self.fl_skipNextGravity = false;
                self.prefix();
                var v5 = self.calcSteps(self.dx, self.dy);
                var v3 = 0;
                while (true) {
                    if (!(!self.fl_stopStepping && v3 < v5.total)) break;
                    var v6 = self.cx;
                    var v7 = self.cy;
                    var v8 = self.fcx;
                    var v9 = self.fcy;
                    var v10 = self.x;
                    var v11 = self.y;
                    self.oldX = self.x;
                    self.oldY = self.y;
                    self.x += v5.dx;
                    self.y += v5.dy;
                    self.updateCoords();
                    if (self.fl_hitWall) {
                        var v12 = false;
                        if (self.dx > 0 && self.world.getCase({x: hf.Entity.x_rtc(v10 + hf.Data.CASE_WIDTH * 0.5), y: hf.Entity.y_rtc(v11)}) > 0) {
                            v12 = true;
                        }
                        if (self.dx < 0 && self.world.getCase({x: hf.Entity.x_rtc(v10 - hf.Data.CASE_WIDTH * 0.5), y: hf.Entity.y_rtc(v11)}) > 0) {
                            v12 = true;
                        }
                        if (v12) {
                            self.x = v10;
                            v5.dx = 0;
                            self.updateCoords();
                            self.onHitWall();
                        }
                    }
                    if (self.fl_hitBorder && (self.x < hf.Data.BORDER_MARGIN || self.x >= hf.Data.GAME_WIDTH - hf.Data.BORDER_MARGIN) || self.fl_hitWall && self.world.getCase({x: self.cx, y: hf.Entity.y_rtc(v11)}) > 0) {
                        self.x = v10;
                        v5.dx = 0;
                        self.updateCoords();
                        self.onHitWall();
                    }
                    if (self.fl_hitWall && v5.dy > 0 && !self.fl_kill) {
                        if (self.world.getCase(hf.Entity.rtc(v10, v11 + Math.floor(hf.Data.CASE_HEIGHT / 2))) != hf.Data.WALL && self.world.getCase({x: self.fcx, y: self.fcy}) == hf.Data.WALL) {
                            self.x = v10;
                            v5.dx = 0;
                            self.updateCoords();
                            self.onHitWall();
                        }
                    }

                    var collidingMovingPlatform: Null<MovingPlatform> = entityCollidesWithAMovingPlatform(self);
                    if (self.fl_hitGround && v5.dy >= 0) {
                        var linkedMovingPlatform: Null<MovingPlatform> = (cast self).linkedMovingPlatform;
                        if ((collidingMovingPlatform != null && (linkedMovingPlatform != collidingMovingPlatform))
                            || self.world.getCase(hf.Entity.rtc(v10, v11 + Math.floor(hf.Data.CASE_HEIGHT / 2))) != hf.Data.GROUND && self.world.getCase({x: self.fcx, y: self.fcy}) == hf.Data.GROUND) {
                            if (collidingMovingPlatform != null || self.world.checkFlag({x: self.fcx, y: self.fcy}, hf.Data.IA_TILE)) {
                                if (self.fl_skipNextGround) {
                                    self.fl_skipNextGround = false;
                                } else {
                                    v5.dy = 0;
                                    self.onHitGround(self.y - self.fallStart);
                                    if (collidingMovingPlatform != null)
                                        self.y = collidingMovingPlatform._y;
                                    self.fallStart = null;
                                    self.updateCoords();
                                }
                            }
                        }
                    }
                    (cast self).linkedMovingPlatform = collidingMovingPlatform;

                    if (self.fl_hitCeil && v5.dy <= 0) {
                        if (self.world.getCase(hf.Entity.rtc(v10, v11 - Math.floor(hf.Data.CASE_HEIGHT / 2))) <= 0 && self.world.getCase(hf.Entity.rtc(self.x, self.y - Math.floor(hf.Data.CASE_HEIGHT / 2))) > 0) {
                            v5.dy = 0;
                            self.onHitCeil();
                            self.updateCoords();
                        }
                    }
                    if (v6 != self.cx || v7 != self.cy) {
                        var v13 = false;
                        if (self.fl_hitGround && v7 < self.cy) {
                            if (self.needsPatch()) {
                                if (self.world.getCase({x: v6, y: v7}) <= 0 && self.dy > 0 && self.world.getCase({x: self.cx, y: self.cy}) > 0 && self.cy < hf.Data.LEVEL_HEIGHT) {
                                    self.x = hf.Entity.x_ctr(v6);
                                    self.y = hf.Entity.y_ctr(v7);
                                    v5.dy = 0;
                                    self.updateCoords();
                                    self.onHitGround(self.y - self.fallStart);
                                    v13 = true;
                                }
                            }
                        }
                        if (self.fl_hitGround && self.dy >= 0 && v6 != self.cx && v7 != self.cy) {
                            if (self.world.getCase({x: v6, y: v7}) <= 0 && self.world.getCase({x: self.cx, y: self.cy}) == hf.Data.GROUND) {
                                self.x = hf.Entity.x_ctr(v6);
                                self.y = hf.Entity.y_ctr(v7);
                                v5.dx = 0;
                                self.updateCoords();
                                self.onHitWall();
                                v13 = true;
                            }
                        }
                        if (!v13) {
                            self.tRem(v6, v7);
                            self.infix();
                            self.updateCoords();
                            self.tAdd(self.cx, self.cy);
                        }
                    }
                    ++v3;
                }
            }
            self.fl_stopStepping = false;
            self.postfix();
            if (self.fl_friction) {
                if ((self.game.fl_ice || self.fl_slide) && self.fl_stable) {
                    if (self.slideFriction == null) {
                        self.dx *= self.game.sFriction;
                    } else {
                        self.dx *= Math.pow(self.slideFriction, hf.Timer.tmod);
                    }
                } else {
                    self.dx *= self.game.xFriction;
                    self.dy *= self.game.yFriction;
                }
            }
            if (Math.abs(self.dx) <= 0.2) {
                self.dx = 0;
            }
            if (Math.abs(self.dy) <= 0.2) {
                self.dy = 0;
            }
            if (self.y >= hf.Data.DEATH_LINE) {
                self.onDeathLine();
            }
        });

        unlinkPlatformFromPlayerOnMove = Ref.auto(Trigger.moveTo).before(function(hf: Hf, self: Trigger, x: Float, y: Float): Void {
            (cast self).linkedMovingPlatform = null;
        });

        removePlatformsOnLevelEnd = Ref.auto(GameMode.clearLevel).before(function(hf: Hf, self: GameMode): Void {
            for (platform in movingPlatforms) {
                /* We can skip destroy, as it would remove it from this array that we clear anyway after. */
                platform.removeMovieClip();
            }
            movingPlatforms = [];
        });
    }
}
