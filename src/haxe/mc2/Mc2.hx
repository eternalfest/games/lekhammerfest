package mc2;

import mc2.actions.Mc2Action;
import merlin.IAction;

@:build(patchman.Build.di())
class Mc2 {
    @:diExport
    public var action(default, null): IAction;

    public function new() {
        this.action = new Mc2Action();
    }
}
