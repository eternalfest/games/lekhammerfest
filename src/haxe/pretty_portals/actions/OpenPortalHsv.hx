package pretty_portals.actions;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import merlin.IActionContext;
import patchman.Assert;
import etwin.Obfu;

class OpenPortalHsv implements IAction {
    public var name(default, null): String = Obfu.raw("openPortalHsv");
    public var isVerbose(default, null): Bool = false;

    public function new() {}

    public function run(ctx: IActionContext): Bool {
        var x: Float = ctx.getFloat(Obfu.raw("x"));
        var y: Float = ctx.getFloat(Obfu.raw("y"));
        var pid: Int = ctx.getInt(Obfu.raw("pid"));
        var h: Float = ctx.getOptFloat(Obfu.raw("h")).toNullable();
        var s: Float = ctx.getOptFloat(Obfu.raw("s")).toNullable();
        var v: Float = ctx.getOptFloat(Obfu.raw("v")).toNullable();

        var game: hf.mode.GameMode = ctx.getGame();

        game.openPortal(x, y, pid);
        var p: hf.mode.GameMode.PortalMc = game.portalMcList[pid];
        Assert.debug(p != null);

        if (h == null) {
            h = 0;
        }
        if (s == null) {
            s = 100;
        }
        if (v == null) {
            v = 100;
        }

        var filter: ColorMatrixFilter = ColorMatrix.fromHsv(h, s / 100, v / 100).toFilter();
        p.mc.filters = [filter];

        return false;
    }
}
