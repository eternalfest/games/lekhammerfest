package pretty_portals;

import pretty_portals.actions.OpenPortalHsv;

import color_matrix.ColorMatrix;
import etwin.flash.filters.ColorMatrixFilter;
import merlin.IAction;
import etwin.ds.WeakMap;
import patchman.IPatch;
import patchman.Ref;


@:build(patchman.Build.di())
class PrettyPortals {
    public static var PORTALS(default, null): WeakMap<hf.mode.GameMode.PortalMc, Float> = new WeakMap();

    public static var PATCH(default, null) = Ref.auto(hf.mode.GameMode.main).before(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        for (p in self.portalMcList) {
            var hue: Null<Float> = PORTALS.get(p);
            if (hue != null) {

                var filter: ColorMatrixFilter = ColorMatrix.fromHsv(hue, 1, 1).toFilter();
                var nextHue: Float = (hue + 1) % 360;

                p.mc.filters = [filter];
                PrettyPortals.PORTALS.set(p, nextHue);
            }
        }
    });

    @:diExport
    public var patch(default, null): IPatch;
    @:diExport
    public var openPortalHsv(default, null): IAction;

    public function new(): Void {
        this.patch = PATCH;
        this.openPortalHsv = new OpenPortalHsv();
    }
}
