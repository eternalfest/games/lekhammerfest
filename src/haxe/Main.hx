import maxmods.VisualEffects;
import maxmods.Misc;
import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import patchman.IPatch;
import patchman.Patchman;
import better_script.NoNextLevel;
import better_script.Players;
import bottom_bar.BottomBar;
import lekhammerfest.Lekhammerfest;
import mc2.Mc2;
import pretty_portals.PrettyPortals;
import magic_library.CustomEntities;
import magic_library.entities.MovingPlatform;
import atlas.Atlas;


@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
      bugfix: Bugfix,
      debug: Debug,
      gameParams: GameParams,
      noNextLevel: NoNextLevel,
      players: Players,
      bottomBar: BottomBar,
      visualEffects: VisualEffects,
      misc: Misc,
      lekhammerfest: Lekhammerfest,
      mc2: Mc2,
      prettyPortals: PrettyPortals,
      customEntities: CustomEntities,
      platformPhysicsOverride: PlatformPhysicsOverride,
      atlasNoFireball: atlas.props.NoFireball,
      atlasDarkness: atlas.props.Darkness,
      atlas: atlas.Atlas,
      merlin: Merlin,
      patches: Array<IPatch>,
      hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
